Style Carousel is a luxury fashion rental service. We offer one-time rentals as well as an innovative subscription offering.

Our service gives women access to their favourite designer brands while eliminating the battle between looking good and being sustainable.

We are always striving to bring you the best so if you have any comments or feedback about our services or platform please email us at feedback@stylecarousel.com. We would love to hear from you!

Website : http://www.stylecarousel.com